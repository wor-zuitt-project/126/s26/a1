//import the contents of the Express package to use for our application 
const express = require("express");


// import Mongoose
const mongoose = require("mongoose")


//give the express() function of the express package the variable "app" so that it can be used 
const app = express();

//Mongoose's connect method takes our MongoDB Atlas connection string and
//use it to connect to MongoDB Atlas

// useNewUrlParser and useUnifiedTopology are both set to true as part of
// a newer Mongoose update that allows a better way of connecting to MongoDB Atlas,
// since the older way is about to be deprecated (become obsolete)



mongoose.connect("mongodb+srv://admin:admin@cluster0.uhxum.mongodb.net/b126_to-do?retryWrites=true&w=majority", {
	
	//These 2 are predifined function
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"))


//Creating a Mongoose Schema

// using the Schema method of Mongoose let us define the rulse that our data must follow to be validated (allowed)


// Schemas determine two things :
// 1. They determine the fields in a given resource
// 2. They determine the data types of each field

// ** resouece = group of data that's important for your website


const taskSchema = new mongoose.Schema({ // the new keyword creates a new schema
	// here we define the fields that MUST be included in each new task as well as 
	// what data type they must be 
	name: String, // all task must have a name and all statuses must be a string
	status: {
		type: String,
		default: "pending" // default values are automatically set
	}
})

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})


// name is the field and string is the Data type of name 



// After hacing defined our schema, we can use it in a Model

// Models use schema and are used to create the actual data that will be saved
//or modified in the database

// If the schema is the list of rules, then the Model enforces those rules



const Task = mongoose.model("Task", taskSchema)

const User = mongoose.model("User", userSchema)

// We paste the taskSchema in model, so it knows what it gonna look like
// Note, we cannot pass many Schema inside a model because we only have 1 priciple for a model

app.use(express.json()); // allows your server to read and send json data
app.use(express.urlencoded({
	extended: true
})); // allow your server to read data from forms



const port = 4000; 



app.get("/", (req, res) => {
	res.send("Hello Jino")
})


app.post("/tasks", (req, res) => {
	console.log(req.body)
	// 

	Task.findOne({name: req.body.name}, (err, result) => {
		// If we find anything, it will store in result on line 91
		// err and result is the parameter	

		console.log(result)
		if(result !== null && result.name === req.body.name) {
			return res.send("Duplicate task found")
		} else {
			// return res.send("No duplicate task found")
			
			//create a new task object
			let newTask = new Task({
				name: req.body.name
			})


			//IF no error, err = null so we get else
			newTask.save((err, task) => {
				if(err){
					return res.send("Err creating a new task")
				} else{
					return res.send("New task success created")
				}
			})
		}
	})
})


//Route to get all tasks
app.get("/tasks", (req, res) => {

	//mongoDB find() without a condition returns All documents
	Task.find({}, (err, result) => {
		if(err){
			return res.send("Error getting tasks.")
		} else {
			//if no error is found, return results as json
			return res.json({
				data: result
			})
		}
	} )
})


// Activity 

app.post("/users", (req, res) => {
	console.log(req.body)

	User.findOne({username: req.body.username}, (err, result) => {
		console.log(result)
		if(result !== null && result.username === req.body.username) {
			return res.send("Duplicate username found")
		} else {
			
			let newUsername = new User({
				username: req.body.username
		})

		newUsername.save((err, mynewUser) => {
				if(err){
					return res.send("Err creating a new username")
				} else{
					return res.send("New username successfully created")
				}
			})

		}

	})
})








// confirm that Atlas connection is successful
app.listen(port, () => console.log(`Server running at port ${port}`))

/* 
1) In app.get, you can change get to post, delete, put, and other HTTP method or Request method

2) "/" is our end point

3) app.listen(port, () => console.log(`Server running at port ${port}`))
 means listent to our port, then console.log(.......)

 */


// new keywords uses to create the object. We can apply it to create schema.


/*

After setting up the connection

1) Create Schema
2) Create Model 
3) Allow JSON and urlencoded (forms to send)
4) Create Route using "app.post()" //Request Method
5) findOne function using Task Schema. Here, err and result are the parameter that 
We will use to check whether the result passes the validation 

Note: Everything always start with creating a new schema - ex. create user we need to crease schema first

*/


/* Activity 

Create a route for creating a new user when a POST request is sent to the /users
end point. If either the username or password field are empty, registeration will not take place(return error message).
Additionally, check for the duplicate username before creating new user
*/